var board;//UI об'єкт
var game;//Об'єкт логіки


window.onload = function () {
    initGame();//загрузка гри
};

var initGame = function() {//налаштування для UI
    var cfg = {
        draggable: true,//перетягування фігури мишкою дозволяється
        position: 'start',//почати дошку з фігур в цьому місці (стартовий стан)
        onDrop: handleMove,//функція, що викликається при скиданні фрагмента
    };

    board = new ChessBoard('gameBoard', cfg);
    game = new Chess();
};

var handleMove = function(source, target ) {//UI дає джерело і ціль і використовує логіку
    var move = game.move({from: source, to: target});//перевіряє переміщення, якщо не перевірити, поверне нуль

    if (move === null)  return 'snapback';//якщо хід нульовий, фігурі не буде дозволено рухатись і він "відскочить назад" у положення до бажаного ходу
};

// налаштування мого сокет-клієнта
var socket = io();

// викликається, коли гравець робить хід на дошці UI
var handleMove = function(source, target) {
    var move = game.move({from: source, to: target});

    if (move === null)  return 'snapback';
    else socket.emit('move', move);//надіслати повідомлення про переміщення всім підключеним клієнтам
};

// викликається, коли сервер викликає socket.broadcast ('переміщення')
socket.on('move', function (msg) {
    game.move(msg);//якщо переміщення клієнтів перевіряється, ініціюйте його
    board.position(game.fen()); // fen - це компонування дошки, оновлення дошки відповідно до стану гри
});
