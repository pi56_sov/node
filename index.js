
const express = require('express');
const app = express();
app.use(express.static('public'));
var http = require('http').Server(app);
const port = process.env.PORT || 8080;
var io = require('socket.io')(http);//socket.io об'єкт, приєднаний до сервера http




app.get('/', (req, res) => res.sendFile(__dirname+ '/public/default.html'));

http.listen(port, () => console.log(`Listening on port ${port}!`));

// встановлення сокет сервера
io.on('connection', function(socket) {
    console.log('new connection');


    // Викликається, коли клієнт викликає socket.emit ('переміщення')
    socket.on('move', function(msg) {//зворотний дзвінок для обробки повідомлень - прослуховування руху
        socket.broadcast.emit('move', msg);//надіслати отримане повідомлення "переміщення" всім, хто знаходиться у підключеному сокеті, за винятком сокета, який щойно його надіслав

    });
});
